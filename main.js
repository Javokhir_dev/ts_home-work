// export {}
var persons = [
    { fName: "Javohir", age: 20, gender: "erkak" },
    { fName: "Ayoub", age: 18, gender: "erkak" },
    { fName: "Mansur", age: 20, gender: "erkak" },
    { fName: "Shaxroyor", age: 18, gender: "erkak" },
    { fName: "Diyor", age: 17, gender: "erkak" },
];
function findPerson(name) {
    var searchUser = persons.find(function (i) { return i.fName === name; });
    if (searchUser) {
        if (searchUser.age >= 18) {
            return "".concat(name, " armiyaga borishi mumkin.");
        }
        else
            return "".concat(name, " hali gudak armiyaga borishi mumkin emas ))");
    }
    else
        return console.warn("".concat(name, " ismli shaxs topilmadi"));
}
console.log(findPerson("Javohir"));
console.log(findPerson("Diyor"));
console.log(findPerson("Maftuna"));
