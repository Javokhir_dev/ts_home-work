// export {}

// let arr2: readonly string[] = ["a", "a", "a", "a", "a"];
// let resultValue = "hello";

// arr2.splice("a", "b")
// arr2.push(resultValue)
// arr2.pop()
// arr2.splice(1,2)

// const turple: [string, number, boolean] = ["Shaxsriyor", 19, false, "Mo'minov"]
// console.log(arr2);
// console.log(turple);
// let tuple: readonly [string, number, boolean] = ["Shaxsriyor", 19, false];
// tuple.push("foziljon")
// console.log(tuple);

// // define our readonly tuple
// const ourReadonlyTuple: readonly [number, boolean, string] = [5, true, 'The Real Coding God'];
// // throws error as it is readonly.
// ourReadonlyTuple.push('Coding God took a day off');
// console.log(ourReadonlyTuple);

// let arr: [width: number, height: number] = [500, 450];
// const [] = arr
// console.log(arr);
// let obj1: { name: string; age: number }
// obj1 =  { name: "yulduz", age: 18 };
// obj1.name ="oy"
// console.log(obj1);

//optional value \
// let obj1: { country?: string; name?: string; teenager?: boolean, };
// obj1 = {name:"yulduz"}
// obj1.teenager = true

// const person: { name: string } = { name: "Adam" };
// person["name"] = "Salom";
// console.log(person);
// for (const key in person) {
//   const element = person[key];
//   console.log(element);
// }
// const person: { [index: string  ]: string } = { name: "Adam" };
// enum roles {
//   USER = "user",
//   ADMIN,
//   DIRECTOR,
//   SALES,
// }

// enum statusCode {
//   NotFound = 404,
//   Success = 200,
//   Accepted = 202,
//   BadRequest = 400,
// }

// console.log(roles[0]);
// console.log(roles.ADMIN);
// console.log(roles.DIRECTOR);
// console.log(roles.SALES);
// type CarYear = number;
// type CarType = string;
// type CarModel = string;

// type Car = {
//   year: CarYear;
//   type: CarType;
//   model: CarModel;
// };

// const CarYear: CarYear = 14;

// console.log(CarYear);

// type MyObjType = {
//   age?: number;
//   name: string;
// };
// let user: MyObjType = {
//   name: "islom",
//   age: 16,
// };

// console.log(user);

// interface IRectangle {
//   width?: number;
//   height: number;
// }
// const maxWidth: IRectangle = {
//   width: 120,
//   height: 100,
// };
// console.log(maxWidth);

interface Users {
  fName: string;
  age: number;
  gender: string;
}

const persons: Users[] = [
  { fName: "Javohir", age: 20, gender: "erkak" },
  { fName: "Ayoub", age: 18, gender: "erkak" },
  { fName: "Mansur", age: 20, gender: "erkak" },
  { fName: "Shaxroyor", age: 18, gender: "erkak" },
  { fName: "Diyor", age: 17, gender: "erkak" },
];

function findPerson(name: string) {
  const searchUser = persons.find((i) => i.fName === name);
  if(searchUser){
    if(searchUser.age >= 18){
      return `${name} armiyaga borishi mumkin.`
    }
    else return `${name} hali gudak armiyaga borishi mumkin emas ))`
  }
  else return console.warn(`${name} ismli shaxs topilmadi`)
}

console.log(findPerson("Javohir"));
console.log(findPerson("Diyor"));
console.log(findPerson("Maftuna"));

